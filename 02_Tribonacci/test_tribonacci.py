import unittest

class Test_test_tribonacci(unittest.TestCase):
    
    def test_tribonacci(self):
        signature = [1,1,1]
        n = 10
        result = []
        if n > 3:
            result = signature
        else:
            result = signature[0:n]
        for i in range(0, n - len(signature)):
            result.append(sum(result[i:3+i]))
        self.assertTrue([1,1,1,3,5,9,17,31,57,105], result)

if __name__ == '__main__':
    unittest.main()
