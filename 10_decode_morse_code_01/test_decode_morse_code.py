import unittest

class Test_test_decode_morse_code(unittest.TestCase):
    def test_decodeMorse(self):
        morse_code = '.... . -.--  .--- ..- -.. .'
        mapping = {".-":"A", "-...":"B", "-.-.":"C", "-..":"D", ".":"E", "..-.":"F", 
                    "--.":"G", "....":"H", "..":"I", ".---":"J", "-.-":"K", ".-..":"L",
                    "--":"M", "-.":"N", "---":"O", ".--.":"P", "--.-":"Q", ".-.":"R", "...":"S",
                    "-":"T", "..-":"U", "...-":"V", ".--":"W", "-..-":"X", "-.--":"Y", "--..":"Z",
                    "-----":"0", ".----":"1", "..---":"2", "...--":"3", "....-":"4", ".....":"5",
                    "-....":"6", "--...":"7", "---..":"8", "----.":"9", ".-.-.-": ".", "--..--": ",",
                    "---...": ":", "..--..":"?", "-...-": "=", "-....-": "-", "-.--.": "(",
                    "-.--.-": ")", ".-..-.": "\"", ".----." : "'", "-..-.": "/", "-.-.--": "!"}
        result = ''
        sos_special = "...---..."
        consecutive_space = 0
        for v in morse_code.split():
            if v == '':
                consecutive_space +=1
            else:
                consecutive_space = 0
            result += mapping.get( v, " " if v == '' and consecutive_space == 1 else v)
        self.assertEqual(result.strip().replace(sos_special, "SOS"), "HEY JUDE")

if __name__ == '__main__':
    unittest.main()