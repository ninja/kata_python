import unittest
import string

class Test_test_Ciphers(unittest.TestCase):
    
    def test_ciphers(self):
        value = "Hello World!"
        alph = list(string.ascii_lowercase)
        count = 0
        result = ''
        for i in value:
            if i.lower() in alph:
                index = alph.index(i.lower())
                if index % 2 == 0:
                    result += "0"
                else:
                    result += "1"
            else:
                result += i
        self.assertTrue('10110 00111!', result)

if __name__ == '__main__':
    unittest.main()
