import unittest

class Test_test_human_readable_time(unittest.TestCase):

    def test_make_readable(self):
        seconds = 60
        s = 0
        m = 0
        h = 0
        for i in range(0,seconds):
            s += 1
            if s == 60:
                m += 1
                s = 0
            if m == 60:
                h += 1
                m = 0
        else:
            if h <= 9:
                h = "0" + str(h)
            if m <= 9:
                m = "0" + str(m)
            if s <= 9:
                s = "0" + str(s)
        return "%s:%s:%s" % (str(h),str(m),str(s))
        self.assertTrue( "00:01:00", result)

if __name__ == '__main__':
    unittest.main()
