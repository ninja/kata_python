import unittest
from collections import Counter

class Test_test_find_it(unittest.TestCase):

    def test_find_it(self):
        seq = [20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]
        dis = [ x for x in seq if x % 2 != 0]
        result = Counter(dis)
        sort = sorted(result.items(),key=self.takesecond, reverse=True)
        for i in sort:
            if i[1] %2 !=0:
                return i[0]
        self.assert_True( sort)

    def takesecond(self, elem):
        return elem[1]

if __name__ == '__main__':
    unittest.main()
