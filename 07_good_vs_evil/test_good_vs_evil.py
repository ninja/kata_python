import unittest

class Test_test_good_vs_evil(unittest.TestCase):

    def test_good_vs_evil(self):
        good = '0 0 0 0 0 10'
        evil = '0 1 1 1 1 0 0'
        good_car =[-1,-2,-3,-3,-4,-10]
        evil_car =[-1,-2,-2,-2,-3,-5,-10]
        result = 0
        for i,j in enumerate(evil.split(' ')):
            if int(j) >= 1:
                result += evil_car[i]
        for i,j in enumerate(good.split(' ')):
            if int(j) >= 1:
                result += good_car[i] * -1
        if result < 0:
            return "Battle Result: Evil eradicates all trace of Good"
        elif result > 0:
            return "Battle Result: Good triumphs over Evil"
        else:
            return "Battle Result: No victor on this battle field"
        self.fail("Not implemented")

if __name__ == '__main__':
    unittest.main()
