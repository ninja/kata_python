import unittest

class Test_test_spin_words(unittest.TestCase):

    def test_spin_words(self):
        sentence = "Hey fellow warriors"
        result = ""
        words = sentence.rsplit(' ')
        for w in words:
            if len(w) >= 5:
                result += w[::-1]
            else:
                result += w
            result += " "
        else:
            result = result[:-1]
        self.assert_True("Hey wollef sroirraw", result)

if __name__ == '__main__':
    unittest.main()
