import unittest

class Test_test1_two_to_one(unittest.TestCase):

    def test_two_to_one(self):
        s1 = "aretheyhere"
        s2 = "yestheyarehere"
        result =[]
        for l in s1+s2:
            if l not in result:
                result.append(l)
        j = ''.join(sorted(result))
        self.assert_True("aehrsty", j)

if __name__ == '__main__':
    unittest.main()
