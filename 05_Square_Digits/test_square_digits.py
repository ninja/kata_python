import unittest

class Test_test_square_digits(unittest.TestCase):

    def test_square_digits(self):
        num = 9119
        every_num = repr( num)
        result = ''
        for n in every_num:
            result += str(int(n) ** 2)
        self.assertTrue(811181, int(result))

if __name__ == '__main__':
    unittest.main()
